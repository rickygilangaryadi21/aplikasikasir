<?php
//memanggil file conn.php yang berisi koneski ke database
//dengan include, semua kode dalam file conn.php dapat digunakan pada file index.php
include('koneksi.php');

$status = '';
$result = '';
$ID_PENJUALAN = "";
$ID_KASIR = "";
$QTY_PENJUALAN = "";
$TOTAL_HARGA_PENJUALAN = "";

//melakukan pengecekan apakah ada variable GET yang dikirim
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['ID_PENJUALAN'])) {
        //query SQL
        $ID_PENJUALAN_upd = $_GET['ID_PENJUALAN'];
        $query = "SELECT * FROM PENJUALAN WHERE ID_PENJUALAN = '$ID_PENJUALAN_upd'";

        //eksekusi query
        $result = mysqli_query($koneksi, $query);
        $data = mysqli_fetch_array($result);
        $ID_PENJUALAN = $data['ID_PENJUALAN'];
        $ID_KASIR = $data['ID_KASIR'];
        $QTY_PENJUALAN = $data['QTY_PENJUALAN'];
        $TOTAL_HARGA_PENJUALAN = $data['TOTAL_HARGA_PENJUALAN'];
    }
}

//melakukan pengecekan apakah ada form yang dipost
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ID_PENJUALAN = $_POST['ID_PENJUALAN'];
    $ID_KASIR = $_POST['ID_KASIR'];
    $QTY_PENJUALAN = $_POST['QTY_PENJUALAN'];
    $TOTAL_HARGA_PENJUALAN = $_POST['TOTAL_HARGA_PENJUALAN'];
    //query SQL
    $sql = "UPDATE PENJUALAN SET ID_KASIR='$ID_KASIR', QTY_PENJUALAN='$QTY_PENJUALAN', TOTAL_HARGA_PENJUALAN='$TOTAL_HARGA_PENJUALAN' WHERE ID_PENJUALAN='$ID_PENJUALAN'";

    //eksekusi query
    $result = mysqli_query($koneksi, $sql);
    if ($result) {
        $status = 'ok';
    } else {
        $status = 'err';
    }

    //redirect ke halaman lain
    header('Location: arsippemasukan.php?status=' . $status);
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Edit Pemasukan</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>

<body>

    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
            <?php

            include('template/sidebar.php');

            ?>

            <?php

            include('template/navbar.php');

            ?>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Edit Pemasukan</h4>

                                </div>
                                <div class="content">
                                    <form action="editpemasukan.php" method="POST">
                                        <div class="row">
                                            <div class="form-group">
                                                <input type="hidden" name="ID_PENJUALAN" class="form-control">
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>ID Biaya</label>
                                                    <input type="text" name="ID_PENJUALAN" class="form-control" value="<?php echo $ID_PENJUALAN ?> " readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>ID KASIR</label>
                                                    <input type="text" name="ID_KASIR" class="form-control" value="<?php echo $ID_KASIR ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Jumlah PENJUALAN</label>
                                                    <input type="text" name="QTY_PENJUALAN" class="form-control" value="<?php echo $QTY_PENJUALAN ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Total Harga Penjualan</label>
                                                    <input type="text" name="TOTAL_HARGA_PENJUALAN" class="form-control" value="<?php echo $TOTAL_HARGA_PENJUALAN ?>">
                                                </div>





                                                <button type="submit" class="btn btn-info btn-fill pull-right">Edit</button>
                                                <a href="arsippemasukan.php" class="btn btn-primary">Kembali</a>
                                                <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy; <script>
                            document.write(new Date().getFullYear())
                        </script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>

        </div>
    </div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>