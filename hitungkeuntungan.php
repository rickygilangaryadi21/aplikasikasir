<?php

function hitung($tanggal, $koneksi){
	$d_tanggal = explode("-", $tanggal); // tanggal dipotong dibagian -
	$bulan = $d_tanggal[0]."-".$d_tanggal[1]; // hasil : YYYY-MM
	$nominal_penjualan = 0;
	$nominal_pembelian = 0;

	// Hitung seluruh pembelian
	$query = "SELECT SUM(TOTAL_HARGA_PENJUALAN) as nom_jual FROM `penjualan` WHERE TANGGAL_PENJUALAN like '%$bulan%'";
	$result = mysqli_query($koneksi, $query);
	if($result){
		$data = mysqli_fetch_assoc($result);
		$nominal_penjualan = $data['nom_jual'] > 0 ? $data['nom_jual']:0;	// bentuk singkat if else
	}else {
        return $status = 'err';
    }

    // Hitung seluruh pembelian
	$query = "SELECT SUM(TOTAL_HARGA_PEM) as nom_beli FROM `pembelian` WHERE TANGGAL_PEMBELIAN like '%$bulan%'";
	$result = mysqli_query($koneksi, $query);
	if($result){
		$data = mysqli_fetch_assoc($result);
		$nominal_pembelian = $data['nom_beli'] > 0 ? $data['nom_beli']:0;	// bentuk singkat if else
	}else {
        return $status = 'err';
    }

	// Cek apa dibulan ini sudah pernah dihitung jika belum pernah lakukan insert, jika pernah lakukan update
	$query = "SELECT * FROM `keuntungan` WHERE TANGGAL_KEUNTUNGAN like '%$bulan%'";
	$result = mysqli_query($koneksi, $query);
	if($result){
		$data = mysqli_fetch_assoc($result);
		if(count($data) <= 0){ // cek ada data tidak
			// belum ada data keuntunga pada bulan tersebut, lakukan insert
			$result = insert_keuntungan($nominal_pembelian, $nominal_penjualan, $bulan, $koneksi);
		}else{
			// sudah ada data keuntunga pada bulan tersebut, lakukan update
			$result = update_keuntungan($nominal_pembelian, $nominal_penjualan, $bulan, $koneksi);
		}

		if(!$result){
			return $status = 'err';
		}
	}else {
        return $status = 'err';
    }

	return $status = 'ok';
}

function insert_keuntungan($beli, $jual, $bulan, $koneksi){
	if($beli > $jual){
		$selisih = $beli - $jual;
		$jenis = "K"; //kurang 
	}else{
		$selisih = $jual - $beli;
		$jenis = "L"; //lebih
	}

	$query = "INSERT INTO `KEUNTUNGAN` ( `ID_KEUNTUNGAN`, `NOMINAL_KEUNTUNGAN`, `TANGGAL_KEUNTUNGAN`, `JENIS`) VALUES(NULL, '$selisih','$bulan','$jenis')";
	$result = mysqli_query($koneksi, $query);

	if($result){
		return true;
	}else{
		return false;
	}
}

function update_keuntungan($beli, $jual, $bulan, $koneksi){
	if($beli > $jual){
		$selisih = $beli - $jual;
		$jenis = "K"; //kurang 
	}else if($beli == $jual){
		$selisih = $jual - $beli;
		$jenis = "B"; //balik modal
	}else{
		$selisih = $jual - $beli;
		$jenis = "L"; //lebih
	}

	$query = "UPDATE `KEUNTUNGAN` SET NOMINAL_KEUNTUNGAN = $selisih, JENIS = '$jenis' WHERE TANGGAL_KEUNTUNGAN like '%$bulan%' ";
	$result = mysqli_query($koneksi, $query);

	if($result){
		return true;
	}else{
		return false;
	}
}

?>