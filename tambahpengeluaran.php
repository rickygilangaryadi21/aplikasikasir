<?php
//memanggil file conn.php yang berisi koneski ke database
//dengan include, semua kode dalam file conn.php dapat digunakan pada file index.php
include 'koneksi.php';
include 'hitungkeuntungan.php';

$status = '';
//melakukan pengecekan apakah ada form yang dipost
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // $ID_PENJUALAN = $_POST['ID_PENJUALAN'];
    $ID_KASIR = $_POST['ID_KASIR'];
    $TANGGAL_PEMBELIAN = date('Y-m-d H:i:sa');
    $QTY_SUPLAI = $_POST['QTY_SUPLAI'];
    $TOTAL_HARGA_PEM = $_POST['TOTAL_HARGA_PEM'];

    //query SQL
    $query = "INSERT INTO PEMBELIAN ( ID_PEMBELIAN, ID_KASIR, TANGGAL_PEMBELIAN, QTY_SUPLAI, TOTAL_HARGA_PEM) VALUES('', '$ID_KASIR','$TANGGAL_PEMBELIAN','$QTY_SUPLAI','$TOTAL_HARGA_PEM')";

    //eksekusi query
    $result = mysqli_query($koneksi, $query);
    if ($result) {
        $status = hitung($TANGGAL_PEMBELIAN, $koneksi);
    } else {
        $status = 'err';
    }

    header('Location: arsippengeluaran.php?status=' . $status);
}

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Tambah Data Pemasukan</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>

<body>

    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
            <?php

            include('template/sidebar.php');

            ?>

            <?php

            include('template/navbar.php');

            ?>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Tambah Data Pengeluaran</h4>

                                </div>
                                <div class="content">
                                    <form action="tambahpengeluaran.php" method="POST">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- <label>ID Penjualan</label> -->
                                                <input type="hidden" name="ID_PEMBELIAN" class="form-control">
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>ID Kasir</label>
                                                    <input type="text" name="ID_KASIR" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Jumlah Pembelian</label>
                                                    <input type="text" name="QTY_SUPLAI" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Total Harga Pembelian</label>
                                                    <input type="text" name="TOTAL_HARGA_PEM" class="form-control">
                                                </div>
                                            </div>
                                        </div>




                                        <button type="submit" class="btn btn-info btn-fill pull-right">Tambah Data</button>
                                        <a href="arsippengeluaran.php" class="btn btn-primary">Kembali</a>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy; <script>
                            document.write(new Date().getFullYear())
                        </script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>

        </div>
    </div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>