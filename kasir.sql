-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Bulan Mei 2020 pada 10.05
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `ID_KASIR` int(11) NOT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASWORD` varchar(50) DEFAULT NULL,
  `TELEFON` varchar(15) DEFAULT NULL,
  `ROLE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`ID_KASIR`, `USERNAME`, `PASWORD`, `TELEFON`, `ROLE`) VALUES
(1, 'admin', 'admin', '0000', 1),
(2, 'kasir', 'kasir', '2222', 0),
(3, 'mifa', 'abi123', '1234', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `biaya_operasional`
--

CREATE TABLE `biaya_operasional` (
  `id_biaya` int(15) NOT NULL,
  `harga_listrik` int(50) NOT NULL,
  `harga_pdam` int(50) NOT NULL,
  `harga_gedung` int(50) NOT NULL,
  `gaji_pegawai` int(50) NOT NULL,
  `tanggal_biaya` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `biaya_operasional`
--

INSERT INTO `biaya_operasional` (`id_biaya`, `harga_listrik`, `harga_pdam`, `harga_gedung`, `gaji_pegawai`, `tanggal_biaya`) VALUES
(5, 2, 2, 2, 2, '2020-03-09 20:04:59'),
(6, 100, 100, 100, 1000, '2020-03-09 20:06:04'),
(7, 100, 100, 100, 1000, '2020-03-09 20:06:16'),
(8, 100, 100, 100, 1000, '2020-03-09 20:07:52'),
(9, 100, 100, 100, 1000, '2020-03-09 20:07:58'),
(10, 100, 100, 100, 1000, '2020-03-09 20:09:27'),
(11, 100, 100, 100, 1000, '2020-03-09 20:09:38'),
(13, 100, 100, 100, 1000, '2020-03-09 20:12:20'),
(14, 100, 100, 100, 1000, '2020-03-09 20:13:02'),
(15, 100, 100, 100, 1000, '2020-03-09 20:13:22'),
(16, 100, 100, 100, 1000, '2020-03-09 20:13:26'),
(17, 100, 100, 100, 500, '2020-03-09 20:13:54'),
(18, 40293290, 320932900, 93309, 23099302, '2020-03-09 20:22:01'),
(19, 923329, 39288392, 238329, 23892389, '2020-03-09 20:23:10'),
(21, 100, 39288392, 93309, 500, '2020-04-25 07:31:33'),
(22, 40293290, 100, 93309, 500, '2020-04-25 08:52:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `ID_DETAIL_PEM` int(11) NOT NULL,
  `ID_BARANG` int(11) DEFAULT NULL,
  `ID_PEMBELIAN` int(11) DEFAULT NULL,
  `HARGA_BELI` int(11) DEFAULT NULL,
  `QTY_PEM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`ID_DETAIL_PEM`, `ID_BARANG`, `ID_PEMBELIAN`, `HARGA_BELI`, `QTY_PEM`) VALUES
(1, 1, 1, 1000, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `ID_DETAIL_PEN` int(11) NOT NULL,
  `ID_BARANG` int(11) DEFAULT NULL,
  `HARGA_JUAL` int(11) DEFAULT NULL,
  `QTY_PEN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keuntungan`
--

CREATE TABLE `keuntungan` (
  `ID_KEUNTUNGAN` int(11) NOT NULL,
  `ID_PENJUALAN` int(11) DEFAULT NULL,
  `ID_PEMBELIAN` int(11) DEFAULT NULL,
  `NOMINAL_KEUNTUNGAN` int(11) DEFAULT NULL,
  `TANGGAL_KEUNTUNGAN` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE `pembelian` (
  `ID_PEMBELIAN` int(11) NOT NULL,
  `ID_KASIR` int(11) DEFAULT NULL,
  `TANGGAL_PEMBELIAN` datetime DEFAULT NULL,
  `QTY_SUPLAI` int(11) DEFAULT NULL,
  `TOTAL_HARGA_PEM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`ID_PEMBELIAN`, `ID_KASIR`, `TANGGAL_PEMBELIAN`, `QTY_SUPLAI`, `TOTAL_HARGA_PEM`) VALUES
(4, 2, '2020-04-27 00:27:03', 2, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `ID_PENJUALAN` int(11) NOT NULL,
  `ID_KASIR` int(11) DEFAULT NULL,
  `TANGGAL_PENJUALAN` datetime DEFAULT NULL,
  `QTY_PENJUALAN` int(11) DEFAULT NULL,
  `TOTAL_HARGA_PENJUALAN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`ID_PENJUALAN`, `ID_KASIR`, `TANGGAL_PENJUALAN`, `QTY_PENJUALAN`, `TOTAL_HARGA_PENJUALAN`) VALUES
(10, 1, '2020-04-26 18:08:57', 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_barang`
--

CREATE TABLE `stock_barang` (
  `ID_BARANG` int(11) NOT NULL,
  `TIPE_BARANG` int(11) DEFAULT NULL,
  `NAMA_BARANG` varchar(50) DEFAULT NULL,
  `QTY_INVENTORY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `stock_barang`
--

INSERT INTO `stock_barang` (`ID_BARANG`, `TIPE_BARANG`, `NAMA_BARANG`, `QTY_INVENTORY`) VALUES
(1, 0, 'air mineral', 10),
(2, 1, 'roti', 6);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID_KASIR`);

--
-- Indeks untuk tabel `biaya_operasional`
--
ALTER TABLE `biaya_operasional`
  ADD PRIMARY KEY (`id_biaya`);

--
-- Indeks untuk tabel `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`ID_DETAIL_PEM`),
  ADD KEY `FK_BARANG_MASUK` (`ID_BARANG`),
  ADD KEY `FK_DETAIL_MEMBELI` (`ID_PEMBELIAN`);

--
-- Indeks untuk tabel `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`ID_DETAIL_PEN`),
  ADD KEY `FK_DETAIL_JUAL` (`ID_BARANG`);

--
-- Indeks untuk tabel `keuntungan`
--
ALTER TABLE `keuntungan`
  ADD PRIMARY KEY (`ID_KEUNTUNGAN`);

--
-- Indeks untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`ID_PEMBELIAN`),
  ADD KEY `FK_NOTA_BELI` (`ID_KASIR`);

--
-- Indeks untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`ID_PENJUALAN`),
  ADD KEY `FK_NOTA_JUAL` (`ID_KASIR`);

--
-- Indeks untuk tabel `stock_barang`
--
ALTER TABLE `stock_barang`
  ADD PRIMARY KEY (`ID_BARANG`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `biaya_operasional`
--
ALTER TABLE `biaya_operasional`
  MODIFY `id_biaya` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `ID_PEMBELIAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `ID_PENJUALAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD CONSTRAINT `FK_BARANG_MASUK` FOREIGN KEY (`ID_BARANG`) REFERENCES `stock_barang` (`ID_BARANG`);

--
-- Ketidakleluasaan untuk tabel `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `FK_DETAIL_JUAL` FOREIGN KEY (`ID_BARANG`) REFERENCES `stock_barang` (`ID_BARANG`);

--
-- Ketidakleluasaan untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `FK_NOTA_BELI` FOREIGN KEY (`ID_KASIR`) REFERENCES `admin` (`ID_KASIR`);

--
-- Ketidakleluasaan untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `FK_NOTA_JUAL` FOREIGN KEY (`ID_KASIR`) REFERENCES `admin` (`ID_KASIR`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
