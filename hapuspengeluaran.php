<?php

include('koneksi.php');

$status = '';
$result = '';
//melakukan pengecekan apakah ada form yang dipost
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['ID_PEMBELIAN'])) {
        //query SQL
        $id_pembelian_upd = $_GET['ID_PEMBELIAN'];
        $query = "DELETE FROM pembelian WHERE ID_PEMBELIAN = '$id_pembelian_upd'";

        //eksekusi query
        $result = mysqli_query($koneksi, $query);

        if ($result) {
            $status = 'ok';
        } else {
            $status = 'err';
        }

        //redirect ke halaman lain
        header('Location: arsippengeluaran.php?status=' . $status);
    }
}
